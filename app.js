var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const cors = require('cors');
const HTMLParser = require('node-html-parser');
const request = require('request')
const fs = require('fs');
var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());

var cookie_set = ''

function slugify(str) {
    return str.replace(/\s+/g, '-')
        .replace('%', 'เปอร์เซนต์')
        .replace(/[^\u0E00-\u0E7F\w\-]+/g, '')
        .replace(/\-\-+/g, '-')
        .replace(/^-+/, '')
        .replace(/-+$/, '');
}

function withZero(num) {
    return (num < 10) ? '0' + num : num;
}

function revDate(divDate, fdate) {
    var date = new Date(fdate.split('-').reverse().join('/'));
    date.setDate(date.getDate() - divDate);
    return withZero(date.getDate()) + '-' + withZero(date.getMonth() + 1) + '-' + date.getFullYear()
}
function revToDate(divDate, fdate) {
    var date = new Date(fdate.split('-').reverse().join('/'));
    date.setDate(date.getDate() - divDate);
    return +date
}
function getJsonStock(bohtml, jsonData, divDate) {
    return new Promise(rest => {
        var dataTable = bohtml.querySelectorAll('#table-stock-results tbody tr')
        dataTable.forEach((element, index) => {
            // if (index > 88) {
            var tdloop = element.querySelectorAll('td')
            var date = ''
            var title = ''
            tdloop.forEach(td => {
                if (td.getAttribute('class') != 'ac bolder') {
                    if (td.getAttribute('class') == 'bolder') {
                        if (td.innerHTML.trim().indexOf('งวดวันที่') != -1) {
                            date = td.innerHTML.replace('งวดวันที่', '').trim()
                            if (date) {
                                jsonData[title][date] = []
                            }
                        } else {
                            if (td.innerHTML.trim() != '') {
                                title = slugify(td.innerHTML.trim())
                                if (title.indexOf('ยี่กี') == -1) {
                                    if (!jsonData[title]) {
                                        jsonData[title] = {}
                                    }
                                }
                            }
                        }
                    }

                    if (td.getAttribute('class') == 'blue bolder ac') {
                        if (td.innerText.trim()) {
                            if (date != '') {
                                jsonData[title][date].push(td.innerText.trim())
                            }
                        }
                    }
                }
            });
            // }

            if (dataTable.length - 1 == index) {
                rest({ ...jsonData })
            }
        });
    })
}

function getStockDataRev(indexx, divDate) {
    return new Promise((rest, reject) => {
        if (indexx > 5) {
            var datahtml = false
            if (fs.existsSync(`dataview/stock/${divDate}.txt`)) {
                datahtml = fs.readFileSync(`dataview/stock/${divDate}.txt`, 'utf-8')
            }
            if (datahtml) {
                rest(HTMLParser.parse(datahtml))
            } else {
                getNewStockHTML(rest);
            }
        } else {
            getNewStockHTML(rest);
        }
    })

    function getNewStockHTML(rest) {
        request.get(`https://www.superlot.com/reports/stock-results?date=${divDate}`, {
            headers: {
                'content-type': 'application/x-www-form-urlencoded',
                'Cookie': cookie_set,
                'Referer': 'https://www.superlot.com/reports/game-results?game_type_id=1',
                'X-Requested-With': 'XMLHttpRequest'
            }
        }, function (error, response, oldhtml) {
            cookie_set = response.headers['set-cookie'];
            var jsonData = JSON.parse(oldhtml);

            var htmlfinal = `<table id="table-stock-results">
                    <thead class="thin-border-bottom">
                        <tr class="bg-black">
                            <th rowspan="2" class="ac">ชนิดหวย</th>
                            <th rowspan="2" class="ac">ชื่องวด</th>
                            <th colspan="3" class="ac">3 ตัวท้าย</th>
                        </tr>
                        <tr>
                        <th class="ac">3 ตัวบน</th>
                        <th class="ac">2 ตัวบน</th>
                        <th class="ac">2 ตัวล่าง</th>
                        </tr>
                    </thead>
                    ${jsonData.html}
                    </table>`;

            fs.writeFileSync(`dataview/stock/${divDate}.txt`, htmlfinal)

            rest(HTMLParser.parse(htmlfinal));
        });
    }
}

app.get(['/lottery-results/:newdate'], (req, res) => {
    res.set('content-type', 'application/json; charset=utf-8')

    var newdate = req.params.newdate;
    var nocache = req.query.nocache ? true : false;
    var data = {
        html: ''
    }
    var cacheHTML = false
    if (fs.existsSync(`dataview/${newdate}.txt`)) {
        cacheHTML = fs.readFileSync(`dataview/${newdate}.txt`, "utf-8")
    }
    var showhtml_success = false
    console.log(cacheHTML, nocache)
    if (!nocache && cacheHTML) {
        showhtml_success = true
        res.json(JSON.parse(cacheHTML))
    } else {
        console.log('get new')
        request.get('https://www.superlot.com/login', {
            headers: {
                'content-type': 'application/x-www-form-urlencoded',
                'Cookie': cookie_set,
            }
        }, (error, response, html) => {
            var root = HTMLParser.parse(html)
            var token = root.querySelector("form input").getAttribute('value')

            cookie_set = response.headers['set-cookie']

            request.post({
                headers: { 'content-type': 'application/x-www-form-urlencoded', 'Cookie': cookie_set, },
                url: 'https://www.superlot.com/login',
                body: `username=MBASAA55D6&password=123456&_token=${token}`
            }, (error, response, html2) => {
                if (html2.indexOf('login-success') != -1) {
                    cookie_set = response.headers['set-cookie']
                    request.get(`https://www.superlot.com/reports/game-results?_=${revToDate(0, newdate)}&game_type_id=1`, {
                        headers: {
                            'content-type': 'application/x-www-form-urlencoded',
                            'Cookie': cookie_set,
                            'Referer': 'https://www.superlot.com/reports/game-results?game_type_id=1',
                            'X-Requested-With': 'XMLHttpRequest'
                        }
                    }, async (error, response, html3) => {

                        cookie_set = response.headers['set-cookie']

                        var bohtml = HTMLParser.parse(html3)

                        var trgovJson = []

                        bohtml.querySelectorAll('#table-gov-results tbody tr').forEach(element => {
                            var tdloop = element.querySelectorAll('td')
                            var jsonData = {
                                title: '',
                                data: []
                            }
                            tdloop.forEach(td => {
                                if (td.getAttribute('class') == 'blue ac') {
                                    jsonData.data.push(td.innerText.trim())
                                } else {
                                    jsonData.title = td.innerHTML.replace('งวดวันที่', '').trim()
                                }
                            });

                            trgovJson.push(jsonData)
                        });
                        var HanoiVVIIPP = await getHanoiVVIIPPAll(newdate);
                        var trstockJson = await getJsonStock(bohtml, [], newdate)

                        if (!nocache) {
                            var dataRawJson = await new Promise(rest => {
                                var data = false
                                if (fs.existsSync(`dataview/json.json`)) {
                                    data = JSON.parse(fs.readFileSync(`dataview/json.json`, 'utf-8'))
                                }
                                var restJson = null
                                if (data) {
                                    restJson = {
                                        gov: trgovJson,
                                        stock: data
                                    }
                                } else {
                                    restJson = {
                                        gov: trgovJson,
                                        stock: trstockJson
                                    }
                                }
                                rest(restJson)
                            })

                            // todb(trstockJson, newdate);
                        } else {
                            var newdataget = await todb(trstockJson, newdate);
                            var dataRawJson = {
                                gov: trgovJson,
                                stock: newdataget
                            }
                        }

                        var govHTML = genGovHTML(newdate, dataRawJson);

                        var tradeHTML = `<div class="row"><div class="col-sm-12 col-xs-12 px-2"><h2 class="title-main text-center">หวยหุ้น</h2></div>`

                        var stockHTML = `<div class="row"><div class="col-sm-12 col-xs-12 px-2"><h2 class="title-main text-center">หวยต่างประเทศ</h2></div>`

                        Object.entries(dataRawJson.stock).forEach(element => {
                            if (element[0].indexOf('หุ้น') != -1) {
                                tradeHTML += `<div class="col-sm-4 col-xs-12 px-2">
                                        <div class="card mb-3" style="max-width: 100%;">
                                        <div class="card-header text-center" id="bgWhite">
                                        <img src="https://www.countryflags.io/${getIcon(element[0])}/flat/32.png">
                                        ${element[0]}
                                        </div>
                                        <div class="table-num" id="">
                                        <table class="table table-bordered table-border-dark table-auto table-nowrap table-hover">
                                        <thead class="thin-border-bottom">
                                            <tr class="info">
                                                <th>วันที่</th>
                                                <th>3 ตัวบน</th>
                                                <th>2 ตัวบน</th>
                                                <th>2 ตัวล่าง</th>
                                            </tr>
                                        </thead>
                                        <tbody>`
                                Object.entries(sortObjJson(element)).forEach((e, i) => {
                                    if (i <= 4) {
                                        tradeHTML += `<tr>
                                                        <td>${e[0]}</td>
                                                        <td>${e[1][0]}</td>
                                                        <td>${e[1][1]}</td>
                                                        <td>${e[1][2]}</td>
                                                    </tr>`
                                    }
                                })
                                tradeHTML += `</tbody>
                                            </table>
                                            </div>
                                            </div>
                                            </div>`
                            } else if (element[0] == 'ออมสิน') {
                                var AAAA = `<div class="col-sm-4 col-xs-12 px-2">
                            <div class="card mb-3" style="max-width: 100%;">
                            <div class="card-header text-center" id="greenColor">
                            <img src="https://www.countryflags.io/th/flat/32.png">
                            ${element[0]}
                            </div>
                            <div class="table-num" id="">
                            <table class="table table-bordered table-border-dark table-auto table-nowrap table-hover">
                            <thead class="thin-border-bottom">
                                <tr class="info">
                                    <th>วันที่</th>
                                    <th>3 ตัวบน</th>
                                    <th>2 ตัวบน</th>
                                    <th>2 ตัวล่าง</th>
                                </tr>
                            </thead>
                            <tbody>`

                                Object.entries(sortObjJson(element)).forEach((e, i) => {
                                    if (i <= 4) {
                                        AAAA += `<tr>
                                            <td>${e[0]}</td>
                                            <td>${e[1][0]}</td>
                                            <td>${e[1][1]}</td>
                                            <td>${e[1][2]}</td>
                                        </tr>`
                                    }
                                })
                                AAAA += `</tbody>
                                </table>
                                </div>
                                </div>
                                </div>`
                                govHTML = govHTML.replace(/AAAA/gm, AAAA)
                            } else if (element[0] == 'หวย-ธกส-BAAC1') {
                                var BAAC1 = `<div class="col-sm-4 col-xs-12 px-2">
                            <div class="card mb-3" style="max-width: 100%;">
                            <div class="card-header text-center" id="greenColor">
                            <img src="https://www.countryflags.io/th/flat/32.png">
                            ${element[0]}
                            </div>
                            <div class="table-num" id="">
                            <table class="table table-bordered table-border-dark table-auto table-nowrap table-hover">
                            <thead class="thin-border-bottom">
                                <tr class="info">
                                    <th>วันที่</th>
                                    <th>3 ตัวบน</th>
                                    <th>2 ตัวบน</th>
                                    <th>2 ตัวล่าง</th>
                                </tr>
                            </thead>
                            <tbody>`

                                Object.entries(sortObjJson(element)).forEach((e, i) => {
                                    if (i <= 4) {
                                        BAAC1 += `<tr>
                                            <td>${e[0]}</td>
                                            <td>${e[1][0]}</td>
                                            <td>${e[1][1]}</td>
                                            <td>${e[1][2]}</td>
                                        </tr>`
                                    }
                                })
                                BAAC1 += `</tbody>
                                </table>
                                </div>
                                </div>
                                </div>`
                                govHTML = govHTML.replace(/BAAC1/gm, BAAC1)
                            } else if (element[0] == 'หวย-ธกส-BAAC2') {
                                var BAAC2 = `<div class="col-sm-4 col-xs-12 px-2">
                            <div class="card mb-3" style="max-width: 100%;">
                            <div class="card-header text-center" id="greenColor">
                            <img src="https://www.countryflags.io/th/flat/32.png">
                            ${element[0]}
                            </div>
                            <div class="table-num" id="">
                            <table class="table table-bordered table-border-dark table-auto table-nowrap table-hover">
                            <thead class="thin-border-bottom">
                                <tr class="info">
                                    <th>วันที่</th>
                                    <th>3 ตัวบน</th>
                                    <th>2 ตัวบน</th>
                                    <th>2 ตัวล่าง</th>
                                </tr>
                            </thead>
                            <tbody>`

                                Object.entries(sortObjJson(element)).forEach((e, i) => {
                                    if (i <= 4) {
                                        BAAC2 += `<tr>
                                            <td>${e[0]}</td>
                                            <td>${e[1][0]}</td>
                                            <td>${e[1][1]}</td>
                                            <td>${e[1][2]}</td>
                                        </tr>`
                                    }
                                })
                                BAAC2 += `</tbody>
                                </table>
                                </div>
                                </div>
                                </div>`
                                govHTML = govHTML.replace(/BAAC2/gm, BAAC2)
                            } else {
                                stockHTML += `<div class="col-sm-4 col-xs-12 px-2">
                            <div class="card mb-3" style="max-width: 100%;">
                            <div class="card-header text-center">
                            <img src="https://www.countryflags.io/${getIcon(element[0])}/flat/32.png">
                            ${element[0]}
                            </div>
                            <div class="table-num" id="">
                            <table class="table table-bordered table-border-dark table-auto table-nowrap table-hover">
                            <thead class="thin-border-bottom">
                                <tr class="info">
                                    <th>วันที่</th>
                                    <th>3 ตัวบน</th>
                                    <th>2 ตัวบน</th>
                                    <th>2 ตัวล่าง</th>
                                </tr>
                            </thead>
                            <tbody>`
                                Object.entries(sortObjJson(element)).forEach((e, i) => {
                                    if (i <= 4) {
                                        stockHTML += `<tr>
                                            <td>${e[0]}</td>
                                            <td>${e[1][0]}</td>
                                            <td>${e[1][1]}</td>
                                            <td>${e[1][2]}</td>
                                        </tr>`
                                    }
                                })
                                stockHTML += `</tbody>
                                </table>
                                </div>
                                </div>
                                </div>`

                                if (element[0] == 'ฮานอย-แดง') {

                                    stockHTML += `<div class="col-sm-4 col-xs-12 px-2">
                                            <div class="card mb-3" style="max-width: 100%;">
                                            <div class="card-header text-center">
                                            <img src="https://www.countryflags.io/vn/flat/32.png">
                                            เวียดนาม(ฮานอย พิเศษ)
                                            </div>
                                            <div class="table-num" id="">
                                            <table class="table table-bordered table-border-dark table-auto table-nowrap table-hover">
                                            <thead class="thin-border-bottom">
                                                <tr class="info">
                                                    <th>วันที่</th>
                                                    <th>3 ตัวบน</th>
                                                    <th>2 ตัวบน</th>
                                                    <th>2 ตัวล่าง</th>
                                                </tr>
                                            </thead>
                                            <tbody>`
                                    Object.entries(HanoiVVIIPP).forEach((e, i) => {
                                        if (i <= 4) {
                                            stockHTML += `<tr>
                                                            <td>${e[0]}</td>
                                                            <td>${e[1].ThreeUp}</td>
                                                            <td>${e[1].TwoUp}</td>
                                                            <td>${e[1].TwoDown}</td>
                                                        </tr>`
                                        }
                                    })
                                    stockHTML += `</tbody>
                                                </table>
                                                </div>
                                                </div>
                                                </div>`

                                }

                            }
                        });

                        stockHTML += `</div>`

                        tradeHTML += `</div>`

                        data.html = govHTML + stockHTML + tradeHTML

                        fs.writeFileSync(`dataview/${newdate}.txt`, JSON.stringify(data))

                        if (!showhtml_success) {
                            res.json(data)
                        }
                    })
                } else {
                    data.html = 'มีการ Login ซ้อน'
                    res.json(data)
                }

            });
        })
    }
})

function getHanoiVVIIPP(newdate) {
    return new Promise(rest => {
        request.post('http://www.xn--q3cshjt0fyf.net/HomePage/GetLottoPinggoShowType', {
            'headers': {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ "DateNow": newdate })
        }, (error, response, html) => {
            var restJson = JSON.parse(html)
            restJson = restJson.filter(x => {
                return x.Lotto_Round == 'เวียดนาม(ฮานอย พิเศษ)'
            })

            if (restJson.length == 0) {
                rest({
                    Lotto_Round: "เวียดนาม(ฮานอย พิเศษ)",
                    ThreeUp: "-",
                    TwoUp: "-",
                    TwoDown: "-",
                    Round_Index: 23,
                    RoundLottoType: 6
                })
            } else {
                rest(...restJson)
            }

        });
    })
}
function getHanoiVVIIPPAll(fdate) {
    return new Promise(async (rest) => {
        var JsonData = {}
        for (let index = 0; index < 5; index++) {
            var datedd = revDate(index, fdate)
            JsonData[datedd] = await getHanoiVVIIPP(datedd)
            if (index == 4) {
                rest(JsonData)
            }
        }
    })
}
function dateToTime(myDate) {
    myDate = myDate.split("-");
    return new Date(myDate[2], myDate[1] - 1, myDate[0]);
}
function sortObjJson(element) {
    return Object.fromEntries(Object.entries(element[1]).sort((a, b) => {
        return dateToTime(b[0]).getTime() - dateToTime(a[0]).getTime();
    }));
}
function getIcon(name) {
    //https://www.countryflags.io/be/flat/64.png
    if (name.indexOf('นิเคอิ') != -1) {
        return 'jp'
    } else if (name.indexOf('ลาว') != -1) {
        return 'la'
    } else if (name.indexOf('ธกส') != -1) {
        return 'th'
    } else if (name.indexOf('มาเลย์') != -1) {
        return 'my'
    } else if (name.indexOf('ไทย') != -1) {
        return 'th'
    } else if (name.indexOf('จีน') != -1) {
        return 'cn'
    } else if (name.indexOf('ดาวน์โจน') != -1) {
        return 'us'
    } else if (name.indexOf('รัสเซีย') != -1) {
        return 'ru'
    } else if (name.indexOf('สิงคโปร์') != -1) {
        return 'sg'
    } else if (name.indexOf('อังกฤษ') != -1) {
        return 'gb'
    } else if (name.indexOf('อินเดีย') != -1) {
        return 'in'
    } else if (name.indexOf('อียิปต์') != -1) {
        return 'eg'
    } else if (name.indexOf('เกาหลี') != -1) {
        return 'kr'
    } else if (name.indexOf('เยอรมัน') != -1) {
        return 'de'
    } else if (name.indexOf('ไต้หวัน') != -1) {
        return 'tw'
    } else if (name.indexOf('ฮั่งเส็ง') != -1) {
        return 'hk'
    } else if (name.indexOf('ฮานอย') != -1) {
        return 'vn'
    } else if (name.indexOf('เวียดนาม') != -1) {
        return 'vn'
    } else {
        return false
    }
}
function genGovHTML(newdate, dataRawJson) {
    var govHTML = `<div class="row">
    <div class="col-sm-12 col-xs-12 px-2"><h2 class="title-main text-center">หวยไทย</h2></div>
                                    <div class="col-sm-12 col-xs-12 px-2">
                                    <div class="card mb-3" style="max-width: 100%;">
                                    <div class="card-header text-center">
                                    <img src="https://www.countryflags.io/th/flat/32.png">
                                    หวยรัฐบาล
                                    </div>
                                    <div class="table-num" id="">
                                    <table class="table table-bordered table-border-dark table-auto table-nowrap table-hover">
                                    <thead class="thin-border-bottom">
                                        <tr class="info">
                                            <th>วันที่</th>
                                            <th>3 ตัวบน</th>
                                            <th>2 ตัวบน</th>
                                            <th>2 ตัวล่าง</th>
                                            <th>3 ตัวล่าง</th>
                                        </tr>
                                    </thead>
                                    <tbody>`;

    dataRawJson.gov.forEach(element => {
        govHTML += `<tr>
                                        <td>${element.title}</td>
                                        <td>${element.data[0]}</td>
                                        <td>${element.data[1]}</td>
                                        <td>${element.data[2]}</td>
                                        <td>${element.data[3]}</td>
                                    </tr>`;
    });

    govHTML += `</tbody>
                                </table>
                                </div>
                                </div>
                                </div>
                                BAAC1
                                BAAC2
                                AAAA
                                </div>`;
    return govHTML;
}

async function todb(trstockJson, fdate) {
    return new Promise(async rest => {
        for (let index = 1; index < 200; index++) {
            var newdate = revDate(index, fdate);
            console.log(newdate, index)
            trstockJson = await getJsonStock(
                await getStockDataRev(index, newdate),
                trstockJson,
                newdate
            );

            if (index == 199) {
                console.log('save new data json', index)
                fs.writeFileSync(`dataview/json.json`, JSON.stringify(trstockJson))
                rest(trstockJson)
            }
        }
    })
}


module.exports = app;
